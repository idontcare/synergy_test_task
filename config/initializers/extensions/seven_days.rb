module Spree
  Order.class_eval do
    def self.seven_day_task
      items=Order.all
      items.each do |item|
        if item.completed_at
          if (item.completed_at+1.days).strftime("%d/%m/%Y")==Time.now.strftime("%d/%m/%Y")
            Emailer.reviews(item).deliver            
          end
        end
      end
    end
  end
end

