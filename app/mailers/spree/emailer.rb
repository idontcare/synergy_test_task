module Spree
  class Emailer < ActionMailer::Base  
    default :from => "synergy.test.task@gmail.com"
    def reviews(order)
      @order=order    
      mail(:to => order.email, :subject => "Review")
    end
  end
end
